package projet5a.clientandroid.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import projet5a.clientandroid.Activity.JoinActivity;
import projet5a.clientandroid.Activity.QuestionActivity;
import projet5a.clientandroid.R;
import projet5a.clientandroid.SocketManager;
import projet5a.clientandroid.model.SocketInfo;

public class QuestionGridAdapter extends BaseAdapter {

    Context context;
    private final int nbAnswers;
    private final int height;
    private final String[] colors = {"#541388","#e3b505","#29bf12","#bc1900","#0a100d","#cf4d6f","#4a6fa5","#ee8434"};
    private final String[] questionNumber = {"A","B","C","D","E","F","G","H"};
    private LayoutInflater layoutInflater;
    private boolean selectedAnswer = false;
    private Button btn;

    public QuestionGridAdapter(Context context, int nbAnswer, int height) {
        this.context = context;
        this.nbAnswers = nbAnswer;
        this.height = height;
        System.out.println("HEIGTH: "+height);
    }

    @Override
    public int getCount() {
        return nbAnswers;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView;

        rowView = layoutInflater.inflate(R.layout.answer_item, null);
        btn =(Button) rowView.findViewById(R.id.answer_btn);
        btn.setBackgroundColor(Color.parseColor(colors[position]));
        btn.setText(questionNumber[position]);
        btn.setHeight(this.getHeight());
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!selectedAnswer){
                    SocketManager.getInstance().giveAnswer(position);
                    selectedAnswer = true;
                    view.setBackgroundColor(Color.parseColor("#ffffff"));                }

            }
        });

        return rowView;
    }

    private int getHeight(){
        int buttonHeigth =0;
        switch (nbAnswers){
            case 1:
            case 2:
                buttonHeigth=this.height;
                break;
            case 3:
            case 4:
                buttonHeigth = this.height/2;
                break;
            case 5:
            case 6:
                buttonHeigth=this.height/3;
                break;
            case 7:
            case 8:
                buttonHeigth=this.height/4;
                break;
        }
        return buttonHeigth;
    }


}
