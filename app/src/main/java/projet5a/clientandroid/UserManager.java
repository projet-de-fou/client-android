package projet5a.clientandroid;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import projet5a.clientandroid.Activity.ConnectActivity;
import projet5a.clientandroid.Activity.HomeActivity;
import projet5a.clientandroid.Activity.ProfileActivity;
import projet5a.clientandroid.model.VolleyCallback;

public class UserManager {
    private int userId =-1;
    private String pseudo;
    private String email;
    private boolean isConnected = false;
    private static final UserManager instance = new UserManager();
    private UserManager(){}

    public static UserManager getInsance(){return instance;}

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public void connectUser(String email, String password,Context c, final VolleyCallback callback) {
        RequestQueue queue = Volley.newRequestQueue(c);
        String url = UrlManager.getUrl() + "/user?mail=" + email + "&password=" + password;
        StringRequest getRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        JSONObject user;
                        try {
                            user = new JSONObject(response);
                            UserManager.getInsance().setUserId(user.getInt("id"));
                            UserManager.getInsance().setPseudo(user.getString("userName"));
                            UserManager.getInsance().setEmail(user.getString("mail"));
                        } catch (Exception e) {
                            Log.d("Error : ", e.toString());
                        }
                        UserManager.getInsance().setConnected(true);
                        callback.onCallback();
                    }
                }
                ,new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error : ", error.toString());
            }
        }

        );
        queue.add(getRequest);
    }

    public void createAccount(String userName,String email, String password,Context c, final VolleyCallback callback){
        RequestQueue queue = Volley.newRequestQueue(c);
        String url = UrlManager.getUrl() + "/user?mail=" + email + "&userName=" + userName + "&password=" + password;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        callback.onCallback();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error : ", error.toString());
                    }
                }
        );
        queue.add(postRequest);
    }

    public void disconnect(Context c, final VolleyCallback callback){
        RequestQueue queue = Volley.newRequestQueue(c);
        String url = UrlManager.getUrl() + "/signout?id=" + this.userId;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        UserManager.getInsance().setConnected(false);
                        callback.onCallback();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error : ", error.toString());
                    }
                }
        );
        queue.add(postRequest);
    }

    public void editProfile(String email,  String userName, String oldPassword,String newPassword,String checkNewPassword, Context c, final VolleyCallback callback){
        RequestQueue queue = Volley.newRequestQueue(c);
        System.out.println(email + " " + userName + " " + oldPassword + " " + newPassword);

        String url = UrlManager.getUrl() + "/user?id=" + UserManager.getInsance().getUserId() + "&mail=" + email + "&userName=" +
                userName + "&oldPassword=" + oldPassword + "&newPassword=" + newPassword;
        StringRequest postRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                       callback.onCallback();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error : ", error.toString());
                    }
                }
        );
        queue.add(postRequest);
    }
}
