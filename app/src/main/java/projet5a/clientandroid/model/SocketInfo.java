package projet5a.clientandroid.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocketInfo {

    @SerializedName("roomCode")
    @Expose
    private String roomCode;
    @SerializedName("host")
    @Expose
    private String host;
    @SerializedName("port")
    @Expose
    private String port;
    @SerializedName("clientId")
    @Expose
    private String clientId;

    public String getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "SocketInfo{" +
                "roomCode='" + roomCode + '\'' +
                ", host='" + host + '\'' +
                ", port='" + port + '\'' +
                ", clientId='" + clientId + '\'' +
                '}';
    }
}