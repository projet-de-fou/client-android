package projet5a.clientandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Answer {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("correct")
    @Expose
    private Boolean correct;

    public Answer(String title, Boolean correct) {
        this.title = title;
        this.correct = correct;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }
}
