package projet5a.clientandroid.model;

public interface VolleyCallback {
    void onCallback();
    void onCallbackError();
}
