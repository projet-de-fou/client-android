package projet5a.clientandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Question {

    @SerializedName("answers")
    @Expose
    private Answer[] answers;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("clientId")
    @Expose
    private String clientId;

    @SerializedName("theme")
    @Expose
    private String theme;


    public Question(Answer[] answers, String title, String theme) {
        this.answers = answers;
        this.title = title;
        this.theme = theme;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Answer[] getAnswers() {
        return answers;
    }

    public void setAnswers(Answer[] answers) {
        this.answers = answers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
