package projet5a.clientandroid.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import projet5a.clientandroid.R;
import projet5a.clientandroid.SocketManager;
import projet5a.clientandroid.UrlManager;
import projet5a.clientandroid.UserManager;
import projet5a.clientandroid.model.Answer;
import projet5a.clientandroid.model.Question;

public class AddDialogFragment extends Dialog implements android.view.View.OnClickListener{

    private final Activity c;
    private Button cancel;
    private Button add;
    private TextView answerA;
    private TextView answerB;
    private TextView answerC;
    private TextView answerD;
    private RadioGroup validAnswerSelector;
    private TextView question_txt;
    private Spinner spinner_theme;

    public AddDialogFragment(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_question);

        cancel = (Button) findViewById(R.id.btn_cancel);
        add = (Button) findViewById(R.id.btn_add_question);
        answerA = (TextView) findViewById(R.id.txt_answerA);
        answerB = (TextView) findViewById(R.id.txt_answerB);
        answerC = (TextView) findViewById(R.id.txt_answerC);
        answerD = (TextView) findViewById(R.id.txt_answerD);
        question_txt = (TextView) findViewById(R.id.txt_question);
        validAnswerSelector = (RadioGroup) findViewById(R.id.radio_group_answer);
        spinner_theme = findViewById(R.id.spin_theme);

        setThemes();

        add.setOnClickListener(this);
        cancel.setOnClickListener(this);
        }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismiss();
                break;
            case R.id.btn_add_question:
                Answer[] answers = new Answer[4];
                answers[0] = new Answer(answerA.getText().toString(),validAnswerSelector.getCheckedRadioButtonId() == R.id.radio_btn_a);
                answers[1] = new Answer(answerB.getText().toString(),validAnswerSelector.getCheckedRadioButtonId() == R.id.radio_btn_b);
                answers[2] = new Answer(answerC.getText().toString(),validAnswerSelector.getCheckedRadioButtonId() == R.id.radio_btn_c);
                answers[3] = new Answer(answerD.getText().toString(),validAnswerSelector.getCheckedRadioButtonId() == R.id.radio_btn_d);
                Question question = new Question(answers,question_txt.getText().toString(),spinner_theme.getSelectedItem().toString());
                SocketManager.getInstance().addQuestionToGame(question);
                if(UserManager.getInsance().isConnected())
                    SendQuestion(question);
                cancel();
                break;
            default:
                break;
        }

    }

    private void SendQuestion(final Question question){
        final Gson gson = new Gson();
        RequestQueue queue = Volley.newRequestQueue(c);
        StringRequest putRequest = new StringRequest(Request.Method.PUT, UrlManager.getUrl()+"/questionHistory",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        System.out.println(response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println( error.toString());
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders()
            {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                //or try with this:
                //headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("question", gson.toJson(question));
                params.put("userId", Integer.toString(UserManager.getInsance().getUserId()));
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody()  {
                String mes = gson.toJson(this.getParams());
                System.out.println(mes);
                byte[] body = new byte[0];
                try {
                    body = mes.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    System.out.println("Unable to gets bytes from JSON" + e.fillInStackTrace());
                }
                return body;
            }
        };
        System.out.println(putRequest);
        queue.add(putRequest);
    }

    public void setThemes(){
        RequestQueue queue = Volley.newRequestQueue(c);
        String url =UrlManager.getUrl()+"/theme";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray= new JSONArray(response);
                            List<String> spinnerArray =  new ArrayList<String>();
                            for (int i=0;i<jsonArray.length();i++)
                                spinnerArray.add(jsonArray.getString(i));
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(c, android.R.layout.simple_spinner_item, spinnerArray);

                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner_theme.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {}
}


