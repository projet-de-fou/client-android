package projet5a.clientandroid;

public class UrlManager {

    private static String url ="http://rochdion.net:8081";

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String newUrl){
        url = newUrl;
    }
}
