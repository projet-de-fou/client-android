package projet5a.clientandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import projet5a.clientandroid.R;
import projet5a.clientandroid.UrlManager;
import projet5a.clientandroid.UserManager;
import projet5a.clientandroid.model.VolleyCallback;

public class ProfileActivity extends AppCompatActivity {
    private static Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        final EditText emailInput = findViewById(R.id.email);
        final EditText pseudoInput = findViewById(R.id.userName);
        final EditText oldPasswordInput = findViewById(R.id.oldPassword);
        final EditText newPasswordInput = findViewById(R.id.newPassword);
        final EditText checkNewPasswordInput = findViewById(R.id.checkNewPassword);
        final Button editProfileButton = findViewById(R.id.buttonEditProfile);
        editProfileButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                modifyProfile(emailInput, pseudoInput, oldPasswordInput, newPasswordInput, checkNewPasswordInput);
            }
        });
        final Button homeButton = findViewById(R.id.buttonHome);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        emailInput.setHint(UserManager.getInsance().getEmail());
        pseudoInput.setHint(UserManager.getInsance().getPseudo());
        ProfileActivity.context = getApplicationContext();
    }

    public void modifyProfile(EditText emailInput, EditText pseudoInput, EditText oldPasswordInput, EditText newPasswordInput, EditText checkNewPasswordInput) {
        final String email = emailInput.getText().toString();
        final String userName = pseudoInput.getText().toString();
        final String oldPassword = oldPasswordInput.getText().toString();
        final String newPassword = newPasswordInput.getText().toString();
        final String checkNewPassword = checkNewPasswordInput.getText().toString();

        if (newPassword.equals(checkNewPassword)) {
            UserManager.getInsance().editProfile(email,userName,oldPassword,newPassword,checkNewPassword,this,
                    new VolleyCallback(){
                        @Override
                        public void onCallback(){

                            Intent intent = new Intent(context, ProfileActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }

                        @Override
                        public void onCallbackError() {

                        }
                    });
        }
    }
}
