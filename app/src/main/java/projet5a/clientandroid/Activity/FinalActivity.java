package projet5a.clientandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import projet5a.clientandroid.R;

public class FinalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        final Button button = findViewById(R.id.btn_quit);
        final TextView scoreView = findViewById(R.id.txt_score);
        final TextView rankView = findViewById(R.id.txt_rank);
        Bundle b = getIntent().getExtras();
        int rank = -1, score=-1; // or other values
        if(b != null){
            rank = b.getInt("rank");
            rankView.setText(""+rank);
            score = b.getInt("score");
            scoreView.setText(""+score);
            System.out.println("RANk: "+rank+" SCORE: "+score);
        }

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context context = JoinActivity.getAppContext();
                Intent intent = new Intent(context, JoinActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


    }
    @Override
    public void onBackPressed() {}

}
