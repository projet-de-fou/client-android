package projet5a.clientandroid.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;

import projet5a.clientandroid.Adapter.QuestionGridAdapter;
import projet5a.clientandroid.R;

public class QuestionActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_question);
        GridView gridView = (GridView) findViewById(R.id.questionGrid);
        Bundle b = getIntent().getExtras();
        int answerCount = -1; // or other values
        if(b != null)
            answerCount = b.getInt("answerCount");
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        QuestionGridAdapter gridAdapter = new QuestionGridAdapter(this, answerCount,metrics.heightPixels);
        gridView.setAdapter(gridAdapter);
    }

    @Override
    public void onBackPressed() {}
}