package projet5a.clientandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import projet5a.clientandroid.R;
import projet5a.clientandroid.SocketManager;
import projet5a.clientandroid.UserManager;
import projet5a.clientandroid.UrlManager;
import projet5a.clientandroid.model.SocketInfo;


public class JoinActivity extends AppCompatActivity {
    private static Context context;
    private Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        gson = new Gson();
        final Button button = findViewById(R.id.buttonJoinRoom);
        final EditText pseudoInput = findViewById(R.id.userName);
        if(UserManager.getInsance().isConnected()){
            pseudoInput.setText(UserManager.getInsance().getPseudo());
            pseudoInput.setEnabled(false);
        }
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                join();
            }
        });
        JoinActivity.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return JoinActivity.context;
    }

    public void join() {
        RequestQueue queue = Volley.newRequestQueue(this);
        final EditText codeInput = findViewById(R.id.codeInput);
        final EditText pseudoInput = findViewById(R.id.userName);

        String codeRoom = codeInput.getText().toString();
        final String username = pseudoInput.getText().toString();

        String url = UrlManager.getUrl() + "/Room/" + codeRoom.toUpperCase();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        SocketInfo socketInfo = gson.fromJson(response, SocketInfo.class);
                        System.out.println("Response is: "+ socketInfo.toString());
                        SocketManager.getInstance().initSocket(socketInfo);
                        SocketManager.getInstance().joinGame(username);
                        setContentView(R.layout.activity_wait_game);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        });
        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
