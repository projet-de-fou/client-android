package projet5a.clientandroid.Activity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import projet5a.clientandroid.Fragment.AddDialogFragment;
import projet5a.clientandroid.R;
import projet5a.clientandroid.SocketManager;

public class WaitQuestionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_question);

        final Button ready_btn = findViewById(R.id.ready_btn);
        final Button add_btn = findViewById(R.id.add_btn);
        final ConstraintLayout layout = findViewById(R.id.wait_question_layout);

        Bundle b = getIntent().getExtras();
        Boolean win = b.getBoolean("win");
        System.out.println(win);
        if(win)
            layout.setBackgroundColor(Color.parseColor("#39ff14"));
        else
            layout.setBackgroundColor(Color.parseColor("#b3000c"));

        ready_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ready();
                v.setEnabled(false);
                add_btn.setEnabled(false);
            }
        });

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDialogFragment cdd=new AddDialogFragment(WaitQuestionActivity.this);
                cdd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(final DialogInterface dialog) {
                        System.out.println("onCancel WORK !");
                        add_btn.setEnabled(false);
                    }
                });
                cdd.show();
            }
        });


    }

    public void ready() {
        SocketManager.getInstance().playerReady();
    }

    @Override
    public void onBackPressed() {}
}
