package projet5a.clientandroid.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.concurrent.atomic.AtomicInteger;

import projet5a.clientandroid.R;

public class WaitStartGameActivity extends AppCompatActivity {
    private Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_start);
        final Handler handler = new Handler();
        final TextView textView = (TextView) findViewById(R.id.textView123);
        final java.util.concurrent.atomic.AtomicInteger n = new AtomicInteger(3);
        final Runnable counter = new Runnable() {
            @Override
            public void run() {
                textView.setText(Integer.toString(n.get()));
                if(n.getAndDecrement() >= 1 )
                    handler.postDelayed(this, 800);
                else {
                    textView.setText("Get Ready !");
                    // start the game
                }
            }
        };
        handler.postDelayed(counter, 1000);
    }

    @Override
    public void onBackPressed() {}
}
