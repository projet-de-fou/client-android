package projet5a.clientandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import projet5a.clientandroid.UrlManager;
import projet5a.clientandroid.R;


public class ChangeUrlActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_url);
        final TextView urlText = findViewById(R.id.urlText);
        urlText.setText(UrlManager.getUrl());
        final Button btnSave = findViewById(R.id.saveUrlBtn);
        final Context context= getApplicationContext();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UrlManager.setUrl(urlText.getText().toString());
                Intent intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }
}
