package projet5a.clientandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Console;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import projet5a.clientandroid.R;
import projet5a.clientandroid.UserManager;
import projet5a.clientandroid.UrlManager;
import projet5a.clientandroid.model.VolleyCallback;

public class ConnectActivity extends AppCompatActivity {
    private static Context context;
    private Gson gson;
    JSONObject user;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        final Button button = findViewById(R.id.buttonConnectUser);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                connect();
            }
        });
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        final Button buttonFB = findViewById(R.id.login_button);
        buttonFB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                facebookConnect();
            }
        });
        ConnectActivity.context = getApplicationContext();
    }

    public void facebookConnect() {
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        /*UserManager.getInsance().setConnected(true);
                        UserManager.getInsance().setPseudo("test");
                        UserManager.getInsance().setEmail("test@test");
                        /*etc...
                        UserManager.getInsance().setConnected(false);*/

                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    @Override
                    public void onCancel() {
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                }

                    @Override
                    public void onError(FacebookException exception) {
                        System.out.println("ERROR CONNECTION");
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void connect() {

        final EditText emailInput = findViewById(R.id.email);
        final EditText passwordInput = findViewById(R.id.password);

        final String email = emailInput.getText().toString();
        final String password = passwordInput.getText().toString();

        System.out.println(email + " " + password);

        UserManager.getInsance().connectUser(email,password,this,
                new VolleyCallback(){
                    @Override
                    public void onCallback(){
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
            }

                    @Override
                    public void onCallbackError() {

                    }
                });

    }
}
