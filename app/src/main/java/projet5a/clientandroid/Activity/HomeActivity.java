package projet5a.clientandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import projet5a.clientandroid.R;
import projet5a.clientandroid.UrlManager;
import projet5a.clientandroid.UserManager;
import projet5a.clientandroid.model.VolleyCallback;

public class HomeActivity extends AppCompatActivity {
    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Boolean connectedUser;
        if (UserManager.getInsance().isConnected()) {
            connectedUser = true;
        } else {
            connectedUser = false;
        }
        Integer id = -1;
        if (UserManager.getInsance().isConnected()) {
            id = UserManager.getInsance().getUserId();
        }
        final Integer userId = id;

        final Button buttonJoinRoom = findViewById(R.id.buttonJoinRoom);
        final Button buttonSignUp = findViewById(R.id.buttonSignUp);
        final Button buttonConnect = findViewById(R.id.buttonConnect);
        final Button buttonDisconnect = findViewById(R.id.buttonDisconnect);
        final Button buttonProfile = findViewById(R.id.buttonProfie);
        final Button buttonUrl= findViewById(R.id.buttonChangeUrl);
        if (connectedUser) {
            buttonJoinRoom.setVisibility(View.VISIBLE);
            buttonSignUp.setVisibility(View.INVISIBLE);
            buttonSignUp.setHeight(0);
            buttonConnect.setVisibility(View.INVISIBLE);
            buttonConnect.setHeight(0);
            buttonDisconnect.setVisibility(View.VISIBLE);
            buttonProfile.setVisibility(View.VISIBLE);
        } else {
            buttonJoinRoom.setVisibility(View.VISIBLE);
            buttonSignUp.setVisibility(View.VISIBLE);
            buttonConnect.setVisibility(View.VISIBLE);
            buttonDisconnect.setVisibility(View.INVISIBLE);
            buttonDisconnect.setHeight(0);
            buttonProfile.setVisibility(View.INVISIBLE);
            buttonProfile.setHeight(0);
        }


        buttonJoinRoom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, JoinActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                //connection();
            }
        });
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, CreateAccountActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        buttonConnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, ConnectActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        buttonProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        buttonUrl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, ChangeUrlActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        final RequestQueue queue = Volley.newRequestQueue(this);
        buttonDisconnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserManager.getInsance().disconnect(context, new VolleyCallback() {
                    @Override
                    public void onCallback() {
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    @Override
                    public void onCallbackError() {

                    }
                });
            }
        });

        HomeActivity.context = getApplicationContext();
    }
}
