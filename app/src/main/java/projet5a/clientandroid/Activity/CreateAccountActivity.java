package projet5a.clientandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import projet5a.clientandroid.R;
import projet5a.clientandroid.UrlManager;
import projet5a.clientandroid.UserManager;
import projet5a.clientandroid.model.VolleyCallback;

public class CreateAccountActivity extends AppCompatActivity {
    private static Context context;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        final Button button = findViewById(R.id.buttonSignUp);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createAccount();
            }
        });
        CreateAccountActivity.context = getApplicationContext();
    }

    public void createAccount() {

        final EditText emailInput = findViewById(R.id.email);
        final EditText pseudoInput = findViewById(R.id.userName);
        final EditText passwordInput = findViewById(R.id.password);

        final String email = emailInput.getText().toString();
        final String userName = pseudoInput.getText().toString();
        final String password = passwordInput.getText().toString();

        System.out.println(email + " " + userName + " " + password);

        UserManager.getInsance().createAccount(userName,email,password,this,
                new VolleyCallback(){
                    @Override
                    public void onCallback(){
                        Intent intent = new Intent(context, ConnectActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                    @Override
                    public void onCallbackError() {

                    }
                });

    }
}
