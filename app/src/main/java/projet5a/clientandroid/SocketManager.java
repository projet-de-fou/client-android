package projet5a.clientandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import projet5a.clientandroid.Activity.FinalActivity;
import projet5a.clientandroid.Activity.JoinActivity;
import projet5a.clientandroid.Activity.QuestionActivity;
import projet5a.clientandroid.Activity.WaitGameActivity;
import projet5a.clientandroid.Activity.WaitQuestionActivity;
import projet5a.clientandroid.Activity.WaitStartGameActivity;
import projet5a.clientandroid.model.Question;
import projet5a.clientandroid.model.SocketInfo;

public class SocketManager {
    private static final SocketManager instance = new SocketManager();
    private Socket mSocket = null;
    private SocketInfo info= null;
    private Gson gson;

    private SocketManager(){this.gson = new Gson();}

    public static SocketManager getInstance(){
        return instance;
    }

    public void initSocket(SocketInfo info){
        try {
            mSocket = IO.socket("http://"+info.getHost()+":"+info.getPort());
            mSocket.on("gameStarts", onStartGame);
            mSocket.on("askForAnswer", onAskForAnswer);
            mSocket.on("questionEnd", onQuestionEnd);
            mSocket.on("endGamePlayer",onEndGame);
            mSocket.connect();
            this.info = info;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


    public void joinGame(String username){
        String message = "{\"clientId\":\""+info.getClientId()+"\",\"playerName\":\""+username+"\"}";
        mSocket.emit("joinGame",message);
        System.out.println("message join game : " + message);

    }

    public void playerReady() {
        String message = "{\"clientId\":\""+info.getClientId()+"\"}";
        mSocket.emit("playerReady",message);
    }

    public void giveAnswer(int answer) {
        if(answer >= 0) {
            String message = "{\"clientId\":\""+info.getClientId()+"\",\"answer\":\""+answer+"\"}";
            mSocket.emit("giveAnswer", message);
            System.out.println("ANSWER: "+answer);
        } else {
            System.out.println("ERROR ANSWER");
        }
    }

    public void addQuestionToGame(Question question){
        question.setClientId(info.getClientId());
        String message = gson.toJson(question);
        mSocket.emit("addQuestion",message);
        System.out.println("message add question to game: " + message);

    }

    private Emitter.Listener onStartGame = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            System.out.println("ON EST PASSE LA ! START GAME");
            Context context = JoinActivity.getAppContext();
            Intent intent = new Intent(context, WaitStartGameActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    };

    private Emitter.Listener onQuestionEnd = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            System.out.println("QUESTION END");
            Boolean win = false;
            JSONObject data = (JSONObject) args[0];
            try {
                win = data.getBoolean("isAnswerCorrect");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Context context = JoinActivity.getAppContext();
            Intent intent = new Intent(context, WaitQuestionActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle b = new Bundle();
            b.putBoolean("win", win);
            intent.putExtras(b);

            context.startActivity(intent);
        }
    };


    private Emitter.Listener onEndGame = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            System.out.println("GAME END");
            int score = -1, rank= -1;
            mSocket.close();
            JSONObject data = (JSONObject) args[0];
            try {
                score = data.getInt("score");
                rank = data.getInt("rank");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Context context = JoinActivity.getAppContext();
            Intent intent = new Intent(context, FinalActivity.class);
            Bundle b = new Bundle();
            b.putInt("score", score);
            b.putInt("rank", rank);
            intent.putExtras(b);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    };


    private Emitter.Listener onAskForAnswer = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            System.out.println("ON EST PASSE LA ! onAsk");
            Context context = JoinActivity.getAppContext();
            JSONObject data = (JSONObject) args[0];
            int answerCount;
            try {
                String answerCountString = data.getString("answerCount");
                answerCount = Integer.parseInt(answerCountString);
            } catch (JSONException e) {
                return;
            }
            Intent intent = new Intent(context, QuestionActivity.class);
            Bundle b = new Bundle();
            b.putInt("answerCount", answerCount); // id
            intent.putExtras(b);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    };
}
