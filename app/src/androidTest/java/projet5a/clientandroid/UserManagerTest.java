package projet5a.clientandroid;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import projet5a.clientandroid.Activity.HomeActivity;
import projet5a.clientandroid.model.VolleyCallback;

import static org.junit.Assert.*;

public class UserManagerTest {

    private final Context appContext = InstrumentationRegistry.getTargetContext();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void connectDisconnectUser() {
        UserManager.getInsance().connectUser("a@b.fr","pass",this.appContext,
                new VolleyCallback(){
                    @Override
                    public void onCallback(){
                        assertTrue("User logged in",true);
                        UserManager.getInsance().disconnect(appContext,
                                new VolleyCallback(){
                                    @Override
                                    public void onCallback(){
                                        assertTrue("User disconnected in",true);
                                    }
                                    @Override
                                    public void onCallbackError() {
                                        assertTrue("User disconnected failed",false);                    }
                                });
                    }
                    @Override
                    public void onCallbackError() {
                        assertTrue("User login failed",false);                    }
                });
    }

    @Test
    public void createAccount() {

    }


    @Test
    public void editProfile() {
        UserManager.getInsance().connectUser("a@b.fr","pass",this.appContext,
                new VolleyCallback(){
                    @Override
                    public void onCallback(){
                        assertTrue("User logged in",true);
                        UserManager.getInsance().editProfile("a@b.fr","user","pass","newpass","newpass",appContext,
                                new VolleyCallback(){
                                    @Override
                                    public void onCallback(){
                                        assertTrue("Profile edited",true);
                                        UserManager.getInsance().editProfile("a@b.fr","user","newpass","pass","pass",appContext,
                                                new VolleyCallback(){
                                                    @Override
                                                    public void onCallback(){
                                                        assertTrue("Profile edited",true);
                                                        UserManager.getInsance().disconnect(appContext,
                                                                new VolleyCallback(){
                                                                    @Override
                                                                    public void onCallback(){
                                                                        assertTrue("User disconnected in",true);
                                                                    }
                                                                    @Override
                                                                    public void onCallbackError() {
                                                                        fail("User disconnected failed");                    }
                                                                });
                                                    }
                                                    @Override
                                                    public void onCallbackError() {
                                                        fail("Error while editing profile");                    }
                                                });
                                    }
                                    @Override
                                    public void onCallbackError() {
                                        fail("Error while editing profile");                    }
                                });
                    }
                    @Override
                    public void onCallbackError() {
                        assertTrue("User login failed",false);                    }
                });
    }
}